import requests
from pprint import pprint as pp
from bs4 import BeautifulSoup
from sqlalchemy import Column, String, Integer, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db import News, session
from bottle import redirect, request, route, run, template
import bayes
from scraputils import get_news
s = session()


def save_database(dicts):
    s = session()
    rows = s.query(News).filter().all()
    bd_labels = []
    for row in rows:
        bd_labels.append(row.title)
    print(bd_labels)
    for current_new in dicts:
        if current_new['title'] not in bd_labels:
            news = News(title=current_new['title'],
                        author=current_new['author'],
                        url=current_new['url'],
                        comments=current_new['comments'])
            s.add(news)
    s.commit()

@route('/news')
def news_list():
    s = session()
    rows = s.query(News).filter(News.label == None).all()
    return template('news_template.tpl', rows=rows)


@route("/add_label")
def add_label():
    label = request.query.label
    id = request.query.id
    s = session()
    items = s.query(News).filter(News.id == id).update({"label": label})
    s.commit()
    redirect('/news')


@route('/update_news')
def update_news():
    dicts = get_news('https://news.ycombinator.com/', 10)
    print(dicts)
    save_database(dicts)
    redirect('/news')

@route('/recomendations')
def news_recomendations():
    s = session()
    rows = s.query(News).filter(News.label != None).all()
    b = bayes.NaiveBayesClassifier()
    b.fit([elem['title'] for elem in list(rows)], [elem['label'] for elem in list(rows)])
    dicts = get_news('https://news.ycombinator.com/', 5)
    y = b.predict([elem['title'] for elem in dicts])
    for elem, elem_y in zip(dicts, y):
        elem['label'] = elem_y
    return template('news_recommendations.tpl', rows=dicts)

run(host='localhost', port=8080)
